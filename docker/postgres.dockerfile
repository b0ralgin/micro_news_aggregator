FROM postgres:9.4.7
MAINTAINER developer@cognibox.com

RUN localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU.UTF-8
ENV LANG ru_RU.utf8

USER postgres
RUN    /etc/init.d/postgresql start &&\
    psql --command "CREATE USER vk WITH SUPERUSER PASSWORD 'vk';" &&\
    createdb -O snapptrics vk

EXPOSE 5432