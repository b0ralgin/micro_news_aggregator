
Пользователь подает на вход адрес новостного сайта или его RSS-ленты и правило парсинга (формат правила — на усмотрение разработчика).

База данных агрегатора начинает автоматически пополняться новостями с этого сайта.

У пользователя есть возможность просматривать список новостей из базы и искать их по подстроке в заголовке новости.

В качестве примера требуется подключить два любых новостных сайта на выбор.

Результат — исходный код агрегатора, а также рабочие адреса и правила парсинга, которые можно подать ему на вход.

Язык — Golang. Хранилище — любая реляционная база данных.


# Сборка
docker-compose build 

#Запуск 
docker-compose up -d 

#Примеры запросов


## Добавление RSS ленты

POST http://localhost:3000/rss HTTP/1.1
content-type: application/json
```json
{
  "url":"https://news.yandex.ru/index.rss",
  "refresh": 10
}
```
### Добавление HTML сайта

POST http://localhost:3000/html HTTP/1.1
content-type: application/json
```json
{
  "url":"https://news.yandex.ru",
  "refresh": 10,
  "link_selector": ".story__title .link",
  "title_selector": ".story__head",
  "content_selector": ".doc__text",
  "publish_selector": ".doc__time"
}
```
### Получение новостей

GET http://localhost:3000/news?page=0 HTTP/1.1
content-type: application/json


### Поиск по новостям

GET http://localhost:3000/?search=Численность школьников&page=0 HTTP/1.1
content-type: application/json
